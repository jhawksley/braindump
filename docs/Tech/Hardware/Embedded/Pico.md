# Pico Quick Ref

-   Source code in top level project
-   `PICO_SDK_PATH` must be set and valid
-   `mkdir build; cd build`
-   `cmake ..`
    -   Debug build: `cmake -DCMAKE_BUILD_TYPE=Debug ..`
-   `make -j4`


## Building OpenOCD2

-   `./configure --enable-ftdi --enable-bcm2835gpio --disable-werror`
-   `make -j2 && sudo make install`

## Running OpenOCD2

-   ```bash
    openocd -f \
    /Users/jhawksley/Projects/pico/rpi/openocd2/tcl/interface/picoprobe.cfg \
    -f /Users/jhawksley/Projects/pico/rpi/openocd2/tcl/target/rp2040.cfg \
    -s tcl
    ```
## Connect and flash binary

-   `arm-none-eabi-gdb TestProject.elf --eval-command "target remote localhost:3333"`
-   `load` - flash
-   `monitor reset init` - reset the chip
-   `continue` - continue
