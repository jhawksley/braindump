# Java

# Tools

## SDKMan

-   [SDKMan](https://sdkman.io/) Main site
-   [SDKMan Usage](https://sdkman.io/usage) manual

### Usage

-   Alias `sdkls='tree ~/.sdkman/candidates -L 2'` - see what's installed.
-   `sdk install java` - install latest version of something
-   `sdk install java 17.0.0-tem` - install a specific version of something
-   `sdk list [thing]` - list all (or thing-specific) available versions of
    something
-   `sdk use java 8.0.242.hs-adpt` - use a specific version of something *for
    this session only*
-   `sdk current` - get current versions of things installed.
-   `sdk default java 17.0.0-tem` - make this version default