# Python

## Quick Links - Official Documentation

- [Documentation]
    - [Library]
    - [Language]
    - [Versioning]
    - [Documenting Code - reStructuredText](https://www.writethedocs.org/guide/writing/reStructuredText/)
- [Pip]

## Cookbooks

## Build and Packaging

- [Packaging Python]
- [Virtual Environments (venv)]

### Prerequisites

- `python3 -m pip install --upgrade build` - install or upgrade the build system
- `python3 -m pip install --upgrade twine` - install or upgrade the uploader
    - `python3 -m pip install --upgrade build twine` - everything in one go

## Build

- `pip3 install -e ".[tests,dev]"` - install deps from a `setup.cfg` file
- `python3 -m venv --copies env` - create a new venv in `env` (`--copies`)
    - `source env/bin/activate` - activate the env
    - `deactivate` - deactivate the env
- `python3 -m build` - build a distribution from source
- `python3 -m twine upload --repository testpypi dist/*` - upload to test repository
- `python3 -m pip install --index-url https://test.pypi.org/simple/ --no-deps example-pkg` - install
  using Pip from the test repository
- `python3 -m pip install -e <path>` - install a source tree into PIP3 as if it was an installed
  package

## Pip3

- `pip3 install --user django==2.2.1` - install specific version of something
- `pip3 install --editable .` - install current package into a venv so it can be
  used as if it was installed (but edits are reflected immediately)

[Documentation]: https://docs.python.org/3/

[Library]: https://docs.python.org/3/library/index.html

[Language]: https://docs.python.org/3/reference/index.html

[Versioning]: https://www.python.org/dev/peps/pep-0440/#version-scheme

[Pip]: https://pip.pypa.io/en/stable/installation/

[Packaging Python]: https://packaging.python.org/tutorials/packaging-projects/

[Virtual Environments (venv)]: https://docs.python.org/3/library/venv.html