# Go

-   [Doc Index](https://golang.org/doc/) • [Language
    Spec](https://golang.org/ref/spec) • [Standard
    Library](https://pkg.go.dev/std)
-   [Awesome Go Libraries](https://github.com/avelino/awesome-go)

## Project

-   [Standardized Project
    Layout](https://github.com/golang-standards/project-layout)
-   [Makefiles for Go
    Projects](https://danishpraka.sh/2019/12/07/using-makefiles-for-go.html)

## Build and Run

-   `go run .` - build (if necessary) a temporary executable and run the project
    in the CWD
-   `go build` - build a runnable executable in the CWD

### Cross-Compiling

-   Set `GOOS` and `GOARCH`
    -   `GOOS=linux GOARCH=amd64 go build`
    -   `GOOS=darwin GOARDCH=amd64 go build`
-   [More info on the
    combinations](https://johnpili.com/golang-cross-platform-build-goos-and-goarch/)

### Modules

-   `go mod init mymod` - create a Go module in the CWD, named `mymod`
-   Use a module in a code `import` statement, then run `go get .` to pull it
    into `go.mod`

### Formatting

-   `go fmt` - format code in the Go style
-   `goimports -l -w .` - clean up imports for go files in CWD
    -   Install with `go install golang.org/x/tools/cmd/goimports@latest`

### Linting and Vetting

-   `go install golang.org/x/lint/golint@latest` - install the linter.
-   `golint ./...` - lint project

Vet can be used to find logical errors.

-   `go vet ./...`

### All-In-One - `golangci-lint`

Source: https://github.com/golangci/golangci-lint

-   Mac: `brew install golangci-lint`
-   `golangci-lint run` - run the suite

## Packaging

-   `go install github.com/rakyll/hey@latest` - Download, build and install a
    given package.