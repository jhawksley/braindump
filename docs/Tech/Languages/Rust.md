# Rust

## Quick Links - Official Documentation 

[Learn Rust - Documentation Home](https://www.rust-lang.org/learn)

* [Stdlib](https://doc.rust-lang.org/std/index.html)
* [Language Rererence Manual](https://doc.rust-lang.org/book/)
* [Compiler Error Index](https://doc.rust-lang.org/error-index.html)
* [Cargo Packaging](https://doc.rust-lang.org/cargo/index.html)
* [RustDoc](https://doc.rust-lang.org/rustdoc/index.html)

## Cookbooks

* [Command Line Cookbook](https://rust-cli.github.io/book/index.html)

## Scaffolding

* `cargo new hello` - Create a new project.

## Dependencies

- Add dep to `Cargo.toml` and run any cargo command.

## Build

* `cargo clean|build|run` - Clean, build (debug target), run the crate entrypoint.
* `cargo build --release` - Build the optimized, stripped target
* `RUST_LOG=DEBUG cargo run` - Switch on all debug messages and run.

## Logging

- `RUST_LOG=critical,fserv::index_route=trace cargo run ...` - run a binary with a global
  and module Rust log level set. See [Rust Log
  Configuration](https://rust-lang-nursery.github.io/rust-cookbook/development_tools/debugging/config_log.html)