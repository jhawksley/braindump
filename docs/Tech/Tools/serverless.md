# Serverless Framework for Lambda

# Caveats

- If `AWS_PROFILE` is set, _but_ a set of temporary STS credentials is in use **from the environment**, you must `unset AWS_PROFILE` or it will go for the static profile credentials, which will fail if 2FA is required.

# Quick Reference

- `serverless deploy` - deploy a Serverless package to AWS
  - `serverless deploy function -f myFunction` - only deploy function changes
- `serverless invoke --function hello` - invoke a Lambda at AWS
  - `serverless invoke local --function hello` - invoke Lambda locally
- `serverless deploy list functions` - list deployed functions
- `serverless remove` - undeploy / remove stack
- 


# Examples

- [AWS Python Lambda example](https://www.serverless.com/examples/aws-python)

# References

- [Serverless.yml
  Reference](https://www.serverless.com/framework/docs/providers/aws/guide/serverless.yml/)
- [AWS Provider and CLI
  Documentation](https://www.serverless.com/framework/docs/providers/aws) 
