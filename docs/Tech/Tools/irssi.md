# IRSSI

Most of this is condensed from [IRSSI Startup](https://irssi.org/documentation/startup/).
Some comes from [IRSSI Quickstart](https://linuxreviews.org/Irssi_quickstart_guide)

# Networks and Servers

* `/NETWORK LIST` - show all configured networks
* `/CONNECT network` - connect to a network
* `/SERVER add -auth -network NetworkName irc.server.net` - add a server to a network

# Channels

* `/j <channel>` - join a channel
* `/c` - clear the window
* `/wi <nick>` - whois \<nick\>
* `/pa` - part (leave) a channel
* `/msg, /m` - send a private message

# Configuration 

* `/RELOAD` - reload configuration from disk
* `/SAVE` - save current configuration changes to disk
* `META-P` and `META-N` - scroll (page) up and down

# Windows

* `/wc` - close a window


# Libera Chat

* `msg alis list SEARCHTERM` - ask Alis to find channels.