# GPG

# Key Management
- `gpg --list-secret-keys --keyid-format=long` - List secret keys 
- `gpg --search-keys [search]` - search for keys
- `gpg --refresh-keys` - refresh all local keys
- `gpg --list-keys` - list all keys

# Signing
- `gpg --detach-sign -a [file]` - sign (separately) a file
- `gpg --clearsign [file]` - clearsign a file
- `gpg -c [file]` - encrypt a file with password
- `gpg -se -r [recipient key] [file]` - encrypt and sign a file for a recipient


