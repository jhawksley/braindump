# Tools for Mac

-   [Virt-Manager] - KVM administration for Linux VMs on Mac. Packaged for mac.
    A working tap for `brew` can be found [here].  Basic instructions:
    ```
    brew tap Krish-sysadmin/homebrew-virt-manager
    brew install virt-manager virt-viewer
    ```

  [Virt-Manager]: https://virt-manager.org/download/
  [here]: https://github.com/Krish-sysadmin/homebrew-virt-manager
