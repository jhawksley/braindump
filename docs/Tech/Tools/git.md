# Git

This page is very much a work-in-progress!

## General Guidelines

### main-only

- [Centralized
  Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows#centralized-workflow)

 This is the workflow where everyone commits to `main`.  Anytime `main` is advanced by someone else, your own commits must be merged.

### branching 

- [Feature Branch
  Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow)

This is the workflow where everyone branches from `main`. An MR is then needed to merge changes back to main.
