# CLI Notebook

This page contains command-line snippets I use often.  It relies on the aliases mentioned at the end of [Kubernetes Environment Setup](environment.md).

# Kubernetes
See which pods are running in the current context's namespace:
```bash
kc get pods
```

... and in all namespaces:
```bash
kc get pods -A
kc get pods --all-namespaces
```

Get services for a given namespace:
```bash
kc --namespace ingress get services
```

Get a shell in a running pod (assumes one container):
```bash
kc exec -ti pod-name -- sh
```

Apply a k8s YAML object to the system (= install or change something):
```bash
kc apply -f ./prometheus.service-ingress.yaml
```

Delete a k8s YAML object from the system (= uninstall something):
```bash
kc delete -f ./prometheus.service-ingress.yaml
```

Apply all the files in a directory:
```bash
kc apply -f <directory>
kc apply -f .
kc apply -f . --recursive # => Descend and apply
```

... and the opposite:
```bash
kc delete -f <directory>
```

Run (and stop) a bare container (not described by a service - will not be restarted by the API server etc.):
```bash
kc run -ti ubuntu --image ubuntu:latest # => terminal, interactive
kc run -ti ubuntu --rm --image ubuntu:latest # => terminal, interactive, delete on exit
kc run ubuntu --image ubuntu:latest # => Daemon
kc delete pod ubuntu
```

Get logs for a container:
```bash
kc logs ubuntu
kc logs -f ubuntu # => follow
```

# Helm

Install an application into a namespace using a helm chart and a `values` file:
```bash
helm install prometheus-monitoring --namespace monitoring -f  values.yaml prometheus-community/kube-prometheus-stack
```

Use `values.yaml` with a Helm chart and render the generated output (but don't apply it): 
```bash
helm template -f values.yaml release-name chart | less
```



Upgrade a chart already in k8s (to reflect changes to `values.yaml`)
```bash
helm upgrade release-name --namespace ns -f values.yaml chart
```

See what charts are installed:
```bash
helm list # => Current namespace
helm list -A # => All namespaces
```