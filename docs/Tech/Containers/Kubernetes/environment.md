# Kubernetes Environment Setup

I've been working with k8s on AWS.  Their offering, EKS, is very good and for a small fee, they'll host the control plane.  Working with k8s can be somewhat tricky, so here are some things I've learned along the way.

# AWS

If you're accessing your cluster in AWS as an Elastic Kubernetes Cluster, you'll need a working AWS command-line.

Run the following command to see if yours is working.  It should complete correctly in all cases.

```bash
aws sts get-caller-identity
```

## Installing and Configuring AWS CLI

I defer to the [AWS Command Line Interface](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html) guide for this.

### MFA

If you or your organization is using *Multi-Factor Authentication* (MFA), you will have been given a software or hardware token device.  This makes the AWS command-line a bit trickier.

The MFA procedure involves using your standard AWS CLI configuration with the AWS Secure Token Service. 

1.  You use your standard configuration with the AWS STS service, and your MFA device, to generate a set of temporary credentials. 
1.  The credentials generated are in the form of the usual environment variables, `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` and an additional `AWS_SESSION_TOKEN`.  Once these have been issued by STS and placed in the environment, the AWS CLI can be used as normal.

This is all a bit of a faff, so I recommend using my [`aws-sts` script](https://gitlab.com/jhawksley/public_scripts) which will perform the required munging for you.

### SSO

If your organization uses AWS Single Sign On, you should still be able to get a working AWS CLI.  See the CLI documentation above for more information.

## Configuring a Kubernetes Environment for EKS

The most useful Kubernetes CLI is `kubectl`.  It supports different contexts (clusters) which you can switch between at will.  To get a contex for your EKS environment, you will need to do:

```bash
aws eks --region YOUR_REGION_NAME update-kubeconfig --name YOUR_CLUSTER_NAME
```

If this is successful, the context will be added to your environment.  You can run `kubectl get pods -A` to see it working.

### Managing EKS Users

The IAM (or AWS-SSO) user who set up the cluster always has automatic access to it.  Other users will need to be set up.  To manage the EKS cluster in the Console, appropriate IAM roles can be added to the user or the user's group directly.  To interact with the API server via `kubectl`, you'll need to edit the AWS-IAM authentication provider's `configmap` in the cluster itself.

See [Managing users or IAM roles for your cluster](https://docs.aws.amazon.com/eks/latest/userguide/add-user-role.html) for more information.


# Useful Utilities and Hacks

These are the things I commonly use to make K8S more friendly.  See also the [Kubernetes CLI Notebook](CLI%20Notebook.md) for more.

* [Kail](https://github.com/boz/kail) - a Kubernetes log tail.  You can dump logs with a variety of selectors, or everything in a given namespace. Very useful, especially when piped through [multitail](https://linux.die.net/man/1/multitail).
* [kubens and kubectx](https://github.com/ahmetb/kubectx) - quickly change the Kubernetes current namespace and context.  These tools take the pain out of doing this with `kubectl`.

# Aliases

I've set the following aliases in my profile to speed things up a bit:

```bash
alias kc="kubectl"
alias kx="kubectx"
alias kn="kubens"
```