# Minikube

Assumes `alias mk=minimuke`.

# Create Cluster

*	`mk status` -> Get the status of all installed MK clusters
*	`mk start --kubernetes-version=v1.21.2 --driver=hyperkit` -> Start a cluster with a given k8s version (building if necessary)
*	`mk delete` -> Delete a cluster