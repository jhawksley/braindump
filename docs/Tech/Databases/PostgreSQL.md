# PostgreSQL Quick Reference

-   [Documentation](https://www.postgresql.org/docs/14/index.html)

## Environment Variables

-   `PGDATA` - defines where the database directory is

## Initialize

-   `initdb -U scott -W .` - initialize a database in the current directory,
    with the given username and prompting for password.

## Starting the DB

-   `pg_ctl -D . -l logfile start` - start the DB located in the current
    directory
