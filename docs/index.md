# Braindump

*"From my brain to your browser in only 3,852 difficult, error-prone steps"*

I've been asked a couple of times for information on various bits and bobs.  Instead of starting a wiki somewhere, I thought I'd just dump things out into this GitLab Pages site. I'll add sections and pages as I go.

# Colophon

This little website is a [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) instance.  These are static websites hosted by GitLab, which are really just git repositories processed into a website by GitLab's CI/CD pipelines.  The source for this one is [here](https://gitlab.com/jhawksley/braindump).